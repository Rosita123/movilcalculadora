package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText txtnum1;
    private EditText txtnum2;
    private EditText txtResultado;
    private Button btnSumar, btnRestar,btnMulti, btnDiv,btnLimpiar, btnCerrar;
    private Operaciones op = new Operaciones();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponents();
    }


    public void initComponents(){
        txtnum1 = findViewById(R.id.txtNum1);
        txtnum2 = findViewById(R.id.txtNum2);
        txtResultado = findViewById(R.id.txtResult);
        btnSumar = findViewById(R.id.btnSuma);
        btnRestar = findViewById(R.id.btnResta);
        btnMulti = findViewById(R.id.btnMult);
        btnDiv = findViewById(R.id.btnDivi);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);
        setEventos();
    }

    public void setEventos(){
        this.btnSumar.setOnClickListener(this);
        this.btnRestar.setOnClickListener(this);
        this.btnMulti.setOnClickListener(this);
        this.btnDiv.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btnSuma:
                if(txtnum1.getText().toString().equals("") || txtnum2.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, "Ingrese los numeros",Toast.LENGTH_SHORT).show();
                }else{
                    op.setNum1(Float.parseFloat(txtnum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtnum2.getText().toString()));
                    txtResultado.setText(Float.toString(op.suma()));
                }
                break;

            case R.id.btnResta:
                if(txtnum1.getText().toString().equals("") || txtnum2.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, "Ingrese los numeros",Toast.LENGTH_SHORT).show();
                }else {
                    op.setNum1(Float.parseFloat(txtnum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtnum2.getText().toString()));
                    txtResultado.setText(Float.toString(op.resta()));
                }
                break;
            case R.id.btnMult:
                if(txtnum1.getText().toString().equals("") || txtnum2.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, "Ingrese los numeros",Toast.LENGTH_SHORT).show();
                }else{
                    op.setNum1(Float.parseFloat(txtnum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtnum2.getText().toString()));
                    txtResultado.setText(Float.toString(op.mult()));
                }
                break;

            case R.id.btnDivi:
                if(txtnum1.getText().toString().equals("") || txtnum2.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, "Ingrese los numeros",Toast.LENGTH_SHORT).show();
                }else {
                    op.setNum1(Float.parseFloat(txtnum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtnum2.getText().toString()));
                    txtResultado.setText(Float.toString(op.div()));
                }
                break;
            case R.id.btnLimpiar:
                txtnum1.setText("");
                txtnum2.setText("");
                txtResultado.setText("");
                break;

            case R.id.btnCerrar:
                finish();
                break;


        }
    }
}
